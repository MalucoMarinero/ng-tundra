'use strict'
fs = require 'fs'
path = require 'path'
mm = require 'minimatch'
wrench = require 'wrench'
{spawn} = require 'child_process'


lrSnippet = require('grunt-contrib-livereload/lib/utils').livereloadSnippet

mountFolder = (connect, dir) ->
  connect.static require('path').resolve(dir)





module.exports = (grunt) ->
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks)

  grunt.initConfig {
    pkg: grunt.file.readJSON('package.json')
    compass:
      example:
        options:
          sassDir: 'src/example/sass'
          cssDir: 'example/css'
    coffee:
      compile:
        options:
          join: true
          sourceMap: true
        files:
          'build/<%= pkg.name %>.js': 'src/ng-tundra/*.coffee'
          'example/<%= pkg.name %>.js': 'src/ng-tundra/*.coffee'
      clapps:
        options:
          join: true
          bare: true
          sourceMap: false
        files:
          'build/<%= pkg.name %>-src2pot.js': 'src/ng-tundra-src2pot/*.coffee'
          'build/<%= pkg.name %>-po2json.js': 'src/ng-tundra-po2json/*.coffee'
      example:
        expand: true
        cwd: 'src/example/coffee'
        src: ['*.coffee']
        dest: 'example/js/'
        ext: '.js'
    watch:
      coffee:
        files: [
          'src/ng-tundra/*.coffee',
          'src/ng-tundra-src2pot/*.coffee',
          'src/ng-tundra-po2json/*.coffee',
        ]
        tasks: ['coffee:compile', 'coffee:clapps']
      srcBuilder:
        files: [
          'src/ng-tundra/*.coffee',
          'src/ng-tundra-src2pot/*.coffee',
          'src/ng-tundra-po2json/*.coffee',
          'src/example/**/*.haml'
        ]
        tasks: ['coffee:compile', 'coffee:clapps', 'hamlpy:example']
      livereload:
        files: [
          'src/ng-tundra/*.coffee',
          'src/ng-tundra-src2pot/*.coffee',
          'src/ng-tundra-po2json/*.coffee',
          'src/example/**/*.haml'
          'src/example/**/*.coffee'
          'src/example/**/*.sass'
          'example/**/*.{html,css,js}'
        ]
        tasks: ['reloadDispatcher:example']
    reloadDispatcher:
      example:
        "**/*.haml" : ['hamlpy:example']
        "**/*.sass" : ['compass:example']
        "**/*.coffee" : ['coffee:compile', 'coffee:clapps', 'coffee:example']
        "**/*.{html,css,js}" : ['livereload']
    hamlpy:
      example:
        src: 'src/example'
        dest: 'example'
    connect:
      options:
        port: 8085
        hostname: 'localhost'
      livereload:
        options:
          middleware: (connect) ->
            return [
              lrSnippet
              mountFolder connect, 'example'
            ]
  }

  grunt.renameTask 'regarde', 'watch'

  grunt.registerMultiTask 'hamlpy', 'Compile HAML Files into HTML', () ->
    options = this.options()
    done = this.async()

    this.files.forEach (f) ->
      console.log "Compiling haml from #{ f.src } to #{ f.dest}"
      dest = f.dest
      dir = path.dirname dest

      f.src.map (srcPath) ->
        files = wrench.readdirSyncRecursive srcPath
        files = files.filter mm.filter "**/*.haml"

        files.forEach (srcRelPath) ->
          destRelPath = srcRelPath.replace '.haml', '.html'
          destRelDir = destRelPath.replace /[^\\\/?%*:|"<>"]+\.html/, ""
          if destRelDir
            if not fs.existsSync "#{ dest }/#{ destRelDir }"
              fs.mkdirSync "#{ dest }/#{ destRelDir }"
          process = spawn 'hamlpy', [
            "#{ srcPath }/#{ srcRelPath }"
            "#{ dest }/#{ destRelPath }"
          ]
          process.stdout.on 'data', (data) -> console.log data.toString()
          process.stderr.on 'data', (data) -> console.error data.toString()
          process.on 'exit', (code) ->
            if code is 0
              console.log "  '#{ srcPath }/#{ srcRelPath }' -> '#{ dest }/#{ destRelPath }'"

  grunt.registerMultiTask 'reloadDispatcher', 'Run tasks based on extensions.', ->
    for pattern, tasks of this.data
      if grunt.regarde.changed.some mm.filter pattern
        grunt.task.run tasks

  grunt.registerTask 'server', ->
    grunt.task.run [
      'connect:livereload'
      'livereload-start'
      'watch:livereload'
    ]

