'use strict'

htmlparser = require "htmlparser"
fs = require 'fs'

rootPath = ""

# Interface:
# arg2 srcDirectory or srcFile
# arg3 outputDirectory or outputFile
processingDirectory = false

if process.argv[2]
  fs.stat process.argv[2], (err, stat) ->
    rootPath = fs.realpathSync process.argv[2]
    if stat.isDirectory()
      processingDirectory = true
      files = fs.readdirSync process.argv[2]
      files = files.map (str) ->
        return rootPath + '/' + str
      files = files.filter (str) ->
        str.match /.po$/
    else
      files = [rootPath]
    outputFiles = files.map (path) ->
      return {
        fileName: nameOutputFile path, rootPath
        json: po2jsonFile path
      }
    outputFiles.forEach (o) ->
      fs.writeFile o.fileName, o.json, "utf-8", ->
        console.log "Finished writing #{ o.fileName }"


nameOutputFile = (path, originalRootPath) ->
  if process.argv[3]
    if processingDirectory
      newPath = path.replace originalRootPath, process.argv[3]
      return newPath.replace /.po$/, ".json"
    else
      return process.argv[3]
  else
    return path.replace /.po$/, ".json"

    



po2jsonFile = (file) ->
  data = fs.readFileSync file, encoding: "utf-8"
  ids = data.match /^msgid.*$/mg
  strs = data.match /^msgstr.*$/mg
  messages = zipMessages ids, strs
  return JSON.stringify messages


zipMessages = (ids, strs) ->
  if ids.length != strs.length
    throw "Error, incorrect number of msgids and strs"
  len = ids.length
  zip = {}
  for index in [0...len]
    msgid = ids[index].replace(/^msgid "(.*)"$/, "$1").replace(/\\"/g, '"')
    msgstr = strs[index].replace(/^msgstr "(.*)"$/, "$1").replace(/\\"/g, '"')
    if msgid and msgstr
      zip[msgid] = msgstr
  return zip

