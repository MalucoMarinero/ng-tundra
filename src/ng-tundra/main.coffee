'use strict'

currentLocale = localStorage.getItem 'ngTundraLocale'

if currentLocale
  header = document.getElementsByTagName('head').item(0)
  loader = document.createElement('script')
  loader.src = "components/angular-complete/i18n/angular-locale_#{ currentLocale }.js"
  header.insertBefore loader, header.firstChild
else
  localStorage.setItem 'ngTundraLocale', 'en-us'
  currentLocale = 'en-us'




console.log 'running ngTundra'


angular.module 'tundra.filters', ['tundra']
angular.module 'tundra.directives', ['tundra']
tundraMod = angular.module 'tundra', ['tundra.filters', 'tundra.directives'] 
tundraMod.constant 'defaultLocaleDir', '/locale/'
tundraMod.provider 'tundra', (defaultLocaleDir) ->
  tundraProvider =
    localeDir: defaultLocaleDir
    setLocaleDir: (newDir) ->
      this.localeDir = newDir
      return this
    $get: ($rootScope, $http, $q) ->
      request = new XMLHttpRequest()
      request.open 'GET', "#{ this.localeDir }/#{ currentLocale }.json", false
      request.send null

      if request.status == 200
        store = JSON.parse request.responseText

        translateFunc = (transId, tag) ->
          tag = if tag then tag + "::" else ""
          search = tag + transId
          if store[search.trim()]
            return store[search.trim()]
          else
            return transId
        translateFunc.store = store
      else
        translateFunc = (transId) ->
          return transId
        translateFunc.store = {}

      $rootScope.changeTundraLocale = (locale) ->
        localStorage.setItem 'ngTundraLocale', locale
        location.reload()

      return translateFunc
  tundraProvider.$get.$inject = ['$rootScope', '$http', '$q']
  return tundraProvider



angular.module('tundra.directives').directive 't_', [
  'tundra', '$locale', (t_, $locale) ->
    return {
      priority: 10
      restrict: "ECA"
      compile: (elem, attr) ->
        tag = attr.tTag or null

        if attr.t_
          if attr[attr.t_]
            elem[0].setAttribute attr.t_, t_(attr[attr.t_], tag)
        else
        elem[0].innerHTML = t_(elem[0].innerHTML, tag)
    }
  ]
