'use strict'

htmlparser = require "htmlparser"
jsparser = require("uglify-js").parser
fs = require 'fs'


rootPath = ""

if process.argv[2]
  fs.realpath process.argv[2], {}, (err, path) ->
    if err
      throw err
    rootPath = path
    getHTMLFiles path, (err, files) ->
      buildTranslationFile files, (translationObj) ->
        textfile = translationObjToPOT translationObj
        if process.argv[3]
          fs.writeFile process.argv[3], textfile, 'utf-8', ->
            console.log "Finished writing .pot file at #{ process.argv[3] }"
        else
          console.log textfile


translationObjToPOT = (translations) ->
  file = ""
  for id, msg of translations
    str = ""
    for loc in msg.locations
      str += "#: #{ loc }\n"
    str += "msgid #{ escapeMsgId(msg.msgId) }\n"
    str += "msgstr #{ escapeMsgId(msg.msgId) }\n\n"
    file += str
  return file


escapeMsgId = (text) ->
  text = text.replace /"/g, '\\"'
  return '"' + text + '"'


getHTMLFiles = (path, cb) ->
  results = []
  fs.readdir path, (err, files) ->
    if err then return cb err
    pending = files.length
    if !pending then return cb null, results
    files.forEach (file) ->
      file = path + '/' + file
      fs.stat file, (err, stat) ->
        if stat && stat.isDirectory()
          getHTMLFiles file, (err, res) ->
            results = results.concat res
            if !--pending then cb null, results 
        else
          if file.match /\.(html|js)$/
            results.push file
          if !--pending then cb null, results 


buildTranslationFile = (fileList, cb) ->
  translationObj = {}
  pending = fileList.length
  fileList.forEach (file) ->
    getTranslationStrings file, (hits) ->
      for hit in hits
        if translationObj[hit.msgId] == undefined
          translationObj[hit.msgId] = {
            msgId: hit.msgId
            locations: [hit.location]
          }
        else
          translationObj[hit.msgId].locations.push hit.location
      if !--pending then cb translationObj


getTranslationStrings = (file, cb) ->
  fs.readFile file, encoding: "utf-8", (err, data) ->
    if file.match /\.html$/
      handler = new htmlparser.DefaultHandler (err, dom) ->
        if err
          console.log "Couldn't parse " + file
        else
          searchForNgTranslate dom, (htmlHits, jsHits) ->
            translations = parseHits htmlHits, jsHits, data, file
            cb translations
      parser = new htmlparser.Parser handler
      parser.parseComplete data
    if file.match /\.js$/
      searchJSforNgTranslate data, (jsHits) ->
        translations = parseHits [], jsHits, data, file
        cb translations



parseHits = (htmlHits, jsHits, fileData, fileName) ->
  translations = []
  for hit in htmlHits
    transObj = 
      msgId: ""
      location: getLocationOfHit hit, fileData, fileName
    if hit.attribs?.t_ and hit.attribs?.t_ != "t_"
      transObj.msgId = hit.attribs[hit.attribs.t_]
    else
      transObj.msgId = convertDomToString hit.children
    transObj.msgId = transObj.msgId.trim()
    if hit.attribs?.t_tag
      transObj.msgId = "#{ hit.attribs.t_tag }::#{ transObj.msgId }"
    translations.push transObj
  for hit in jsHits
    ast = jsparser.parse(hit)
    transObj =
      msgId: convertAstToMsgId ast
      location: getLocationOfJsHit hit, fileData, fileName
    translations.push transObj
  return translations


convertAstToMsgId = (ast) ->
  calls = recurseAst ast
  call = calls[0]
  if call.length == 1
    return call[0][1]
  else
    return "#{ call[1][1] }::#{ call[0][1] }"


recurseAst = (ast) ->
  calls = []
  if ast[0] == 'toplevel'
    for leaf in ast[1]
      calls = calls.concat recurseAst(leaf)
  if ast[0] == 'stat'
    calls = calls.concat recurseAst(ast[1])
  if ast[0] == 'call'
    if ast[1][1] == 't_' or ast[1][1] == 'tundra'
      calls.push ast[2]
  return calls


getLocationOfJsHit = (hit, fileData, file) ->
  shortFileName = file.replace rootPath, ""
  lineNumber = fileData.split(hit)[0].split('\n').length
  return "#{ shortFileName }:#{ lineNumber }"


getLocationOfHit = (hit, fileData, file) ->
  searchString = convertDomToString [hit]
  shortFileName = file.replace rootPath, ""
  lineNumber = fileData.split(searchString)[0].split('\n').length
  return "#{ shortFileName }:#{ lineNumber }"


convertDomToString = (domTree) ->
  string = ""
  for elem in domTree
    if elem.type == 'text'
      string += elem.data
    else if elem.type == 'tag'
      string += "<#{ elem.data }>"
      if elem.children
        string += convertDomToString elem.children
      string += "</#{ elem.name }>"
  return string


searchForNgTranslate = (domTree, cb) ->
  htmlHits = []
  jsHits = []
  for elem in domTree
    if elem.name == 't_'
      htmlHits.push elem
    if elem.name == 'script' and elem.children
      searchJSforNgTranslate convertDomToString(elem.children), (resJs) ->
        jsHits = jsHits.concat resJs
    if elem.attribs
      if elem.attribs["t_"]
        htmlHits.push elem
      if elem.attribs['class']
        classes = elem.attribs['class'].split ' '
        if classes.indexOf('t_') != -1
          htmlHits.push elem
    if elem.children
      searchForNgTranslate elem.children, (resHtml, resJs) ->
        htmlHits = htmlHits.concat resHtml
        jsHits = jsHits.concat resJs
  cb htmlHits, jsHits


searchJSforNgTranslate = (jsString, cb) ->
  matches = jsString.match /(t_|tundra)\(["'](.*)['"](, ['"](.*)['"]\)|\))/g
  if not matches
    matches = []
  cb matches







