'use strict'

app = angular.module 'ngTundraExample', ['tundra']
app.config (tundraProvider) ->
  tundraProvider.setLocaleDir '/locale'

javascriptCtrl = ($scope, t_) ->
  $scope.translatedString = t_ "Translate in Javascript"
  $scope.uniqueTranslatedString = t_ "Overlapping Translation in Javascript", "uniqueTag"

javascriptCtrl.$inject = ['$scope', 'tundra']
app.controller 'javascriptCtrl', javascriptCtrl

