# ngTundra
## Translation for AngularJS Applications

ngTundra is an end to end setup for translating AngularJS applications entirely on the client side. It provides directives for translating strings in your HTML templates, as well as a service for doing the same in your Javascript. Once you're done, you can run the node app, `ngTundra-src2pot` to collect all of those strings into an industry standard .po message file, and then translate it using whatever tool you like. After that, you can run `ngTundra-po2json` to turn all of those .po files into .json that ngTundra can read for translating your application.

It's also very new, so it's still very much untested in practical usage. It's also undocumented, but that'll certainly be coming. You can check out the [example page](http://malucomarinero.github.io/ng-tundra/) for a full demonstration, and the source is in the example folder of this repo.
  
  
  
  
### MIT License for Open Source

Copyright (c) 2013 Full and By Design

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

