'use strict';
var fs, htmlparser, nameOutputFile, po2jsonFile, processingDirectory, rootPath, zipMessages;

htmlparser = require("htmlparser");

fs = require('fs');

rootPath = "";

processingDirectory = false;

if (process.argv[2]) {
  fs.stat(process.argv[2], function(err, stat) {
    var files, outputFiles;

    rootPath = fs.realpathSync(process.argv[2]);
    if (stat.isDirectory()) {
      processingDirectory = true;
      files = fs.readdirSync(process.argv[2]);
      files = files.map(function(str) {
        return rootPath + '/' + str;
      });
      files = files.filter(function(str) {
        return str.match(/.po$/);
      });
    } else {
      files = [rootPath];
    }
    outputFiles = files.map(function(path) {
      return {
        fileName: nameOutputFile(path, rootPath),
        json: po2jsonFile(path)
      };
    });
    return outputFiles.forEach(function(o) {
      return fs.writeFile(o.fileName, o.json, "utf-8", function() {
        return console.log("Finished writing " + o.fileName);
      });
    });
  });
}

nameOutputFile = function(path, originalRootPath) {
  var newPath;

  if (process.argv[3]) {
    if (processingDirectory) {
      newPath = path.replace(originalRootPath, process.argv[3]);
      return newPath.replace(/.po$/, ".json");
    } else {
      return process.argv[3];
    }
  } else {
    return path.replace(/.po$/, ".json");
  }
};

po2jsonFile = function(file) {
  var data, ids, messages, strs;

  data = fs.readFileSync(file, {
    encoding: "utf-8"
  });
  ids = data.match(/^msgid.*$/mg);
  strs = data.match(/^msgstr.*$/mg);
  messages = zipMessages(ids, strs);
  return JSON.stringify(messages);
};

zipMessages = function(ids, strs) {
  var index, len, msgid, msgstr, zip, _i;

  if (ids.length !== strs.length) {
    throw "Error, incorrect number of msgids and strs";
  }
  len = ids.length;
  zip = {};
  for (index = _i = 0; 0 <= len ? _i < len : _i > len; index = 0 <= len ? ++_i : --_i) {
    msgid = ids[index].replace(/^msgid "(.*)"$/, "$1").replace(/\\"/g, '"');
    msgstr = strs[index].replace(/^msgstr "(.*)"$/, "$1").replace(/\\"/g, '"');
    if (msgid && msgstr) {
      zip[msgid] = msgstr;
    }
  }
  return zip;
};
