'use strict';
var buildTranslationFile, convertAstToMsgId, convertDomToString, escapeMsgId, fs, getHTMLFiles, getLocationOfHit, getLocationOfJsHit, getTranslationStrings, htmlparser, jsparser, parseHits, recurseAst, rootPath, searchForNgTranslate, searchJSforNgTranslate, translationObjToPOT;

htmlparser = require("htmlparser");

jsparser = require("uglify-js").parser;

fs = require('fs');

rootPath = "";

if (process.argv[2]) {
  fs.realpath(process.argv[2], {}, function(err, path) {
    if (err) {
      throw err;
    }
    rootPath = path;
    return getHTMLFiles(path, function(err, files) {
      return buildTranslationFile(files, function(translationObj) {
        var textfile;

        textfile = translationObjToPOT(translationObj);
        if (process.argv[3]) {
          return fs.writeFile(process.argv[3], textfile, 'utf-8', function() {
            return console.log("Finished writing .pot file at " + process.argv[3]);
          });
        } else {
          return console.log(textfile);
        }
      });
    });
  });
}

translationObjToPOT = function(translations) {
  var file, id, loc, msg, str, _i, _len, _ref;

  file = "";
  for (id in translations) {
    msg = translations[id];
    str = "";
    _ref = msg.locations;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      loc = _ref[_i];
      str += "#: " + loc + "\n";
    }
    str += "msgid " + (escapeMsgId(msg.msgId)) + "\n";
    str += "msgstr " + (escapeMsgId(msg.msgId)) + "\n\n";
    file += str;
  }
  return file;
};

escapeMsgId = function(text) {
  text = text.replace(/"/g, '\\"');
  return '"' + text + '"';
};

getHTMLFiles = function(path, cb) {
  var results;

  results = [];
  return fs.readdir(path, function(err, files) {
    var pending;

    if (err) {
      return cb(err);
    }
    pending = files.length;
    if (!pending) {
      return cb(null, results);
    }
    return files.forEach(function(file) {
      file = path + '/' + file;
      return fs.stat(file, function(err, stat) {
        if (stat && stat.isDirectory()) {
          return getHTMLFiles(file, function(err, res) {
            results = results.concat(res);
            if (!--pending) {
              return cb(null, results);
            }
          });
        } else {
          if (file.match(/\.(html|js)$/)) {
            results.push(file);
          }
          if (!--pending) {
            return cb(null, results);
          }
        }
      });
    });
  });
};

buildTranslationFile = function(fileList, cb) {
  var pending, translationObj;

  translationObj = {};
  pending = fileList.length;
  return fileList.forEach(function(file) {
    return getTranslationStrings(file, function(hits) {
      var hit, _i, _len;

      for (_i = 0, _len = hits.length; _i < _len; _i++) {
        hit = hits[_i];
        if (translationObj[hit.msgId] === void 0) {
          translationObj[hit.msgId] = {
            msgId: hit.msgId,
            locations: [hit.location]
          };
        } else {
          translationObj[hit.msgId].locations.push(hit.location);
        }
      }
      if (!--pending) {
        return cb(translationObj);
      }
    });
  });
};

getTranslationStrings = function(file, cb) {
  return fs.readFile(file, {
    encoding: "utf-8"
  }, function(err, data) {
    var handler, parser;

    if (file.match(/\.html$/)) {
      handler = new htmlparser.DefaultHandler(function(err, dom) {
        if (err) {
          return console.log("Couldn't parse " + file);
        } else {
          return searchForNgTranslate(dom, function(htmlHits, jsHits) {
            var translations;

            translations = parseHits(htmlHits, jsHits, data, file);
            return cb(translations);
          });
        }
      });
      parser = new htmlparser.Parser(handler);
      parser.parseComplete(data);
    }
    if (file.match(/\.js$/)) {
      return searchJSforNgTranslate(data, function(jsHits) {
        var translations;

        translations = parseHits([], jsHits, data, file);
        return cb(translations);
      });
    }
  });
};

parseHits = function(htmlHits, jsHits, fileData, fileName) {
  var ast, hit, transObj, translations, _i, _j, _len, _len1, _ref, _ref1, _ref2;

  translations = [];
  for (_i = 0, _len = htmlHits.length; _i < _len; _i++) {
    hit = htmlHits[_i];
    transObj = {
      msgId: "",
      location: getLocationOfHit(hit, fileData, fileName)
    };
    if (((_ref = hit.attribs) != null ? _ref.t_ : void 0) && ((_ref1 = hit.attribs) != null ? _ref1.t_ : void 0) !== "t_") {
      transObj.msgId = hit.attribs[hit.attribs.t_];
    } else {
      transObj.msgId = convertDomToString(hit.children);
    }
    transObj.msgId = transObj.msgId.trim();
    if ((_ref2 = hit.attribs) != null ? _ref2.t_tag : void 0) {
      transObj.msgId = "" + hit.attribs.t_tag + "::" + transObj.msgId;
    }
    translations.push(transObj);
  }
  for (_j = 0, _len1 = jsHits.length; _j < _len1; _j++) {
    hit = jsHits[_j];
    ast = jsparser.parse(hit);
    transObj = {
      msgId: convertAstToMsgId(ast),
      location: getLocationOfJsHit(hit, fileData, fileName)
    };
    translations.push(transObj);
  }
  return translations;
};

convertAstToMsgId = function(ast) {
  var call, calls;

  calls = recurseAst(ast);
  call = calls[0];
  if (call.length === 1) {
    return call[0][1];
  } else {
    return "" + call[1][1] + "::" + call[0][1];
  }
};

recurseAst = function(ast) {
  var calls, leaf, _i, _len, _ref;

  calls = [];
  if (ast[0] === 'toplevel') {
    _ref = ast[1];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      leaf = _ref[_i];
      calls = calls.concat(recurseAst(leaf));
    }
  }
  if (ast[0] === 'stat') {
    calls = calls.concat(recurseAst(ast[1]));
  }
  if (ast[0] === 'call') {
    if (ast[1][1] === 't_' || ast[1][1] === 'tundra') {
      calls.push(ast[2]);
    }
  }
  return calls;
};

getLocationOfJsHit = function(hit, fileData, file) {
  var lineNumber, shortFileName;

  shortFileName = file.replace(rootPath, "");
  lineNumber = fileData.split(hit)[0].split('\n').length;
  return "" + shortFileName + ":" + lineNumber;
};

getLocationOfHit = function(hit, fileData, file) {
  var lineNumber, searchString, shortFileName;

  searchString = convertDomToString([hit]);
  shortFileName = file.replace(rootPath, "");
  lineNumber = fileData.split(searchString)[0].split('\n').length;
  return "" + shortFileName + ":" + lineNumber;
};

convertDomToString = function(domTree) {
  var elem, string, _i, _len;

  string = "";
  for (_i = 0, _len = domTree.length; _i < _len; _i++) {
    elem = domTree[_i];
    if (elem.type === 'text') {
      string += elem.data;
    } else if (elem.type === 'tag') {
      string += "<" + elem.data + ">";
      if (elem.children) {
        string += convertDomToString(elem.children);
      }
      string += "</" + elem.name + ">";
    }
  }
  return string;
};

searchForNgTranslate = function(domTree, cb) {
  var classes, elem, htmlHits, jsHits, _i, _len;

  htmlHits = [];
  jsHits = [];
  for (_i = 0, _len = domTree.length; _i < _len; _i++) {
    elem = domTree[_i];
    if (elem.name === 't_') {
      htmlHits.push(elem);
    }
    if (elem.name === 'script' && elem.children) {
      searchJSforNgTranslate(convertDomToString(elem.children), function(resJs) {
        return jsHits = jsHits.concat(resJs);
      });
    }
    if (elem.attribs) {
      if (elem.attribs["t_"]) {
        htmlHits.push(elem);
      }
      if (elem.attribs['class']) {
        classes = elem.attribs['class'].split(' ');
        if (classes.indexOf('t_') !== -1) {
          htmlHits.push(elem);
        }
      }
    }
    if (elem.children) {
      searchForNgTranslate(elem.children, function(resHtml, resJs) {
        htmlHits = htmlHits.concat(resHtml);
        return jsHits = jsHits.concat(resJs);
      });
    }
  }
  return cb(htmlHits, jsHits);
};

searchJSforNgTranslate = function(jsString, cb) {
  var matches;

  matches = jsString.match(/(t_|tundra)\(["'](.*)['"](, ['"](.*)['"]\)|\))/g);
  if (!matches) {
    matches = [];
  }
  return cb(matches);
};
