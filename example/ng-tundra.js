//@ sourceMappingURL=ng-tundra.map
(function() {
  'use strict';
  var currentLocale, header, loader, tundraMod;

  currentLocale = localStorage.getItem('ngTundraLocale');

  if (currentLocale) {
    header = document.getElementsByTagName('head').item(0);
    loader = document.createElement('script');
    loader.src = "components/angular-complete/i18n/angular-locale_" + currentLocale + ".js";
    header.insertBefore(loader, header.firstChild);
  } else {
    localStorage.setItem('ngTundraLocale', 'en-us');
    currentLocale = 'en-us';
  }

  console.log('running ngTundra');

  angular.module('tundra.filters', ['tundra']);

  angular.module('tundra.directives', ['tundra']);

  tundraMod = angular.module('tundra', ['tundra.filters', 'tundra.directives']);

  tundraMod.constant('defaultLocaleDir', '/locale/');

  tundraMod.provider('tundra', function(defaultLocaleDir) {
    var tundraProvider;

    tundraProvider = {
      localeDir: defaultLocaleDir,
      setLocaleDir: function(newDir) {
        this.localeDir = newDir;
        return this;
      },
      $get: function($rootScope, $http, $q) {
        var request, store, translateFunc;

        request = new XMLHttpRequest();
        request.open('GET', "" + this.localeDir + "/" + currentLocale + ".json", false);
        request.send(null);
        if (request.status === 200) {
          store = JSON.parse(request.responseText);
          translateFunc = function(transId, tag) {
            var search;

            tag = tag ? tag + "::" : "";
            search = tag + transId;
            if (store[search.trim()]) {
              return store[search.trim()];
            } else {
              return transId;
            }
          };
          translateFunc.store = store;
        } else {
          translateFunc = function(transId) {
            return transId;
          };
          translateFunc.store = {};
        }
        $rootScope.changeTundraLocale = function(locale) {
          localStorage.setItem('ngTundraLocale', locale);
          return location.reload();
        };
        return translateFunc;
      }
    };
    tundraProvider.$get.$inject = ['$rootScope', '$http', '$q'];
    return tundraProvider;
  });

  angular.module('tundra.directives').directive('t_', [
    'tundra', '$locale', function(t_, $locale) {
      return {
        priority: 10,
        restrict: "ECA",
        compile: function(elem, attr) {
          var tag;

          tag = attr.tTag || null;
          if (attr.t_) {
            if (attr[attr.t_]) {
              elem[0].setAttribute(attr.t_, t_(attr[attr.t_], tag));
            }
          } else {

          }
          return elem[0].innerHTML = t_(elem[0].innerHTML, tag);
        }
      };
    }
  ]);

}).call(this);
