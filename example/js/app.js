(function() {
  'use strict';
  var app, javascriptCtrl;

  app = angular.module('ngTundraExample', ['tundra']);

  app.config(function(tundraProvider) {
    return tundraProvider.setLocaleDir('/locale');
  });

  javascriptCtrl = function($scope, t_) {
    $scope.translatedString = t_("Translate in Javascript");
    return $scope.uniqueTranslatedString = t_("Overlapping Translation in Javascript", "uniqueTag");
  };

  javascriptCtrl.$inject = ['$scope', 'tundra'];

  app.controller('javascriptCtrl', javascriptCtrl);

}).call(this);
